#include <stdio.h>

double sqrt_of_int(int number, double guess, double precision) {
    double abs = number - guess*guess >= 0 ?  number - guess*guess : guess*guess - number;
    if(abs <= precision) {
        return guess;
    } else {
        return sqrt_of_int(number, 0.5 * (guess + number/guess), precision);
    }
}

int main() {
    double precision = 0.000001;

    int number_to_sqrt = 0;
    printf("Enter number you want to know the square root of: ");
    scanf("%d", &number_to_sqrt);
    printf("The square root of %d approx. is %f\n", number_to_sqrt, sqrt_of_int(number_to_sqrt, number_to_sqrt, precision));

    return 0;
}