#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TEXT_LENGTH 8

typedef char word[TEXT_LENGTH];

typedef struct mmbr {
	word name;
	struct mmbr *next;
} member;

typedef member* linkedList;


// zacatek podprogramu zavislych na nasi konkretni definici clenu seznamu

/**
 * Vytvori seznam z dat ctenych ze zadaneho streamu. 
 *     Prvni text ze souboru ci z klavesnice bude prvni ve spojovem seznamu, 
 *         druhy bude za ni atd. az posledni ze souboru ci z klavesnice bude posledni
 *
 * @param header adresa, z niz se bere anebo do niz se ma ulozit ukazatel na head - hlavu - prvniho clena seznamu
 * @param f_stream proud (soubor), z niz se nac (muze byt stdin)
 */
void load(linkedList *header, FILE *f_stream) {
	linkedList moving;
	word input_word;

	int i = 0;

	*header = moving = NULL;

	do {
		if(i > 0) {
			if(i == 1) {
				moving = (linkedList) malloc(sizeof(member));
				*header = moving;
			} else {
				moving->next = (linkedList) malloc(sizeof(member));
				moving = moving->next;
			}

			strcpy(moving->name, input_word);
		}

		printf("Zadejte jmeno: ");
		fgets(input_word, TEXT_LENGTH, f_stream);
		*(input_word + strlen(input_word) - 1) = '\0';

		i++;
	} while (strlen(input_word) >= 1);

	if(!moving && !header) {
		moving->next = NULL;
	}
}

/**
 * Vypise seznam na terminal
 *
 * @param header adresa, z niz se bere anebo do niz se ma ulozit ukazatel na head - hlavu - prvniho clena seznamu
 */
void print(linkedList *header) {
	linkedList head = *header;
	while(head) {
		printf("%s\n", head->name);
		head = head->next;
	}
}

/**
 * Hleda v seznamu prvni vyskyt clena se zadanym obsahem datove polozky
 *
 * @param header adresa, z niz se bere anebo do niz se ma ulozit ukazatel na head - hlavu - prvniho clena seznamu
 * @param name - znakovy retezec, ktery se ma hledat v polozce name datove structury member
 * @return  ukazatel na nalezeneho clena; nenalezl-li se NULL
 */
linkedList search(linkedList *header, char *name) {
	while(*header) {
		if(strcmp((*header)->name, name) == 0) {
			return *header;
		}

		*header = (*header)->next;
	}

	return NULL;
}

// konec podprogramu zavislych na nasi konkretni definici clenu seznamu 

// zacatek podprogramu nezavislych na nasi konkretni definici clenu seznamu

/**
 * Prida do seznamu z prvek p pred clena seznamu r
 *
 * @param header adresa, z niz se bere anebo do niz se ma ulozit ukazatel na head - hlavu - prvniho clena seznamu
 * @param insert  datova structura member
 * @param before  datova structura member
 */
void insert_before(linkedList *header, linkedList insert, linkedList before) {
	linkedList head = *header;
	linkedList last = NULL;

	while(head) {
		if(head->next == before->next) {
			linkedList end = insert;
			if(!end) { break; }

			while(end->next) {
				end = end->next;
			}

			if(last) {
				end->next = before;
				last->next = insert;
			} else {
				end->next = before;
				*header = insert;
			}

			break;
		}

		last = head;
		head = head->next;
	}
}

/**
 * Prida do seznamu z prvek p za clena seznamu r
 *
 * @param header adresa, z niz se bere anebo do niz se ma ulozit ukazatel na head - hlavu - prvniho clena seznamu
 * @param insert  datova structura member
 * @param after  datova structura member
 */
void insert_after(linkedList *header, linkedList insert, linkedList after) {
	linkedList head = *header;

	if(!head) {
		*header = insert; 
		return;
	}
	
	while(head) {
		if(head->next == after->next) {
			linkedList end = insert;
			if(!end) { break; }

			while(end->next) {
				end = end->next;
			}

			end->next = after->next;
			after->next = insert;

			break;
		}

		head = head->next;
	}
}

/**
 * Vyjme ze seznamu z prvek clena seznamu r
 *
 * @param header adresa, z niz se bere anebo do niz se ma ulozit ukazatel na head - hlavu - prvniho clena seznamu
 * @param remove  datova structura member
 */
void pop(linkedList *header, linkedList remove) {
	linkedList head = *header;
	linkedList last = NULL;

	while(head) {
		if(head->next == remove->next) {
			if(last) {
				last->next = remove->next;
			} else {
				*header = head->next;
			}

			free(remove);

			break;
		}

		last = head;
		head = head->next;
	}
}

/**
 * Vymaze cely seznamu - i. e. vsechny jeho cleny
 *
 * @param header adresa, z niz se bere anebo do niz se ma ulozit ukazatel na head - hlavu - prvniho clena seznamu
 */
void destroy(linkedList *header) {
	linkedList head = *header;
	linkedList last = NULL;

	while(head) {
		if(last) {
			free(last);
		}

		last = head;
		head = head->next;
	}
	
	if(last) {
		free(last);
	}
}