#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>


#define SOURCE_LINE_LENGTH 256
#define NAME_LENGTH 128

int main() {
	FILE * source_file, * first_new_file, * second_new_file;
	int new_line_length;

	printf("Zadejte jmeno zdrojoveho souboru s priponou: ");
	char filename[NAME_LENGTH];
	scanf("%s",filename);
	
	if((source_file = fopen(filename, "r")) == NULL) {
		printf("Nepodarilo se otevrit soubor \"%s\"\n", filename);
    	return EXIT_FAILURE;  
	}
	
	printf("Zadejte jmeno 1. prekladoveho souboru s priponou: ");
	scanf("%s",filename);
	
	if((first_new_file = fopen(filename, "w+")) == NULL) {
		printf("Nepodarilo se otevrit soubor \"%s\"\n", filename);
    	return EXIT_FAILURE;  
	}

	printf("Zadejte jmeno 2. prekladoveho souboru s priponou: ");
	scanf("%s",filename);
	
	if((second_new_file = fopen(filename, "w+")) == NULL) {
		printf("Nepodarilo se otevrit soubor \"%s\"\n", filename);
    	return EXIT_FAILURE;  
	}
	

	printf("Zadejte delku radku v novych souborech: ");
	scanf("%i", &new_line_length);

	char source_line[SOURCE_LINE_LENGTH];

	int i = 1;

	int chars_used_first = 0;
	int chars_used_second = 0;

	bool first_ends_on_nl = false;
	bool second_ends_on_nl = false;

	while(fgets(source_line, SOURCE_LINE_LENGTH, source_file) != NULL) {
		char * new_line = (char *) malloc(sizeof(char) * new_line_length);
		memset(new_line, '\0', sizeof new_line);

		if(strlen(source_line) == 1) {
			strcpy(new_line, "\n");

			if(!first_ends_on_nl) {
				fputs(new_line, first_new_file);
			}

			if(!second_ends_on_nl) {
				fputs(new_line, second_new_file);
			}

			memset(new_line, '\0', sizeof new_line);

			strcpy(new_line, "   ");
			fputs(new_line, first_new_file);
			fputs(new_line, second_new_file);

			chars_used_first = 3;
			chars_used_second = 3;

			continue;
		}

		source_line[strlen(source_line) - 1] = '\0';


		int chars_used;
		bool ends_on_nl;

		if(i % 2 == 1) {
			chars_used = chars_used_first;
			ends_on_nl = first_ends_on_nl;
		} else {
			chars_used = chars_used_second;
			ends_on_nl = second_ends_on_nl;
		}

		do {
			int offset = 1 + chars_used;
			int shift = 0;

			strncpy(new_line, source_line, new_line_length - offset);
			memset(new_line, '\0', new_line_length-1);

			while(!isspace(source_line[new_line_length - offset])) {						
				shift++;
				offset++;
			}

			strncpy(new_line, source_line, new_line_length - offset);

			if((strlen(new_line) + chars_used + shift) >= (new_line_length - 1)) {
				strcat(new_line, "\n");
				chars_used = 0;
				ends_on_nl = true;
			} else {
				chars_used += strlen(new_line);
				ends_on_nl = false;
			}

			shift = 0;

			if(i % 2 == 1) {
				fputs(new_line, first_new_file);
				chars_used_first = chars_used;
				first_ends_on_nl = ends_on_nl;
			} else {
				fputs(new_line, second_new_file);
				chars_used_second = chars_used;
				second_ends_on_nl = ends_on_nl;
			}

			strcpy(source_line, source_line + strlen(new_line));

			memset(new_line, '\0', new_line_length-1);
		} while (strlen(source_line) > 1);

		memset(new_line, '\0', new_line_length - 1);
		free(new_line);
		
		i++;
	}
	
	fclose(source_file);
	fclose(first_new_file);
	fclose(second_new_file);

	return EXIT_SUCCESS;
}