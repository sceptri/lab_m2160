Trdlo, 

   ach, Trdlo, Trdlo! Proc jsi Trdlo? 
    Sve jmeno zapri, zrekni se otce, 
   anebo, nechces-li, odevzej se mne, 
      a ja prestanu byt Kapuletova. 
      Jen Tve jmeno je muj nepritel. 
            Ty jsi jenom ty. 
         Ty vubec nejsi Montek. 
            Co je to Montek? 
Ruka ne, ani noha, ani paze, ani oblicej, 
    ani jina cast, patrici k cloveku.
        Proc nemas jine jmeno?
          Copak je po jmene?
      Co ruzi zvou i jinak zvano
           vonelo by stejne.
   A tak i Trdlo, nebyt Trdlo jmenovan 
      by mi nebyl o nic mene drahy 
           nez s tim jmenem.
         Trdlo svlec to jmeno!
    A za ne, ktere neni casti Tebe, 
             si vezmi mne!

                         Tva Bobes
