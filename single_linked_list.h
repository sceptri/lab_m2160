#pragma once
#include "definedType.h"
#include <stdlib.h>

typedef struct type_member {
	valueType value;
	struct type_member * next;
} member;

typedef member* singleLinkedList;


/**
 * Prida do seznamu z prvek p pred clena seznamu r
 *
 * @param header adresa, z niz se bere anebo do niz se ma ulozit ukazatel na head - hlavu - prvniho clena seznamu
 * @param insert  datova structura member
 * @param before  datova structura member
 */
void insert_before(singleLinkedList *header, singleLinkedList insert, singleLinkedList before) {
	singleLinkedList head = *header;
	singleLinkedList last = NULL;

	while(head) {
		if(head->next == before->next) {
			singleLinkedList end = insert;
			if(!end) { break; }

			while(end->next) {
				end = end->next;
			}

			if(last) {
				end->next = before;
				last->next = insert;
			} else {
				end->next = before;
				*header = insert;
			}

			break;
		}

		last = head;
		head = head->next;
	}
}

/**
 * Prida do seznamu z prvek p za clena seznamu r
 *
 * @param header adresa, z niz se bere anebo do niz se ma ulozit ukazatel na head - hlavu - prvniho clena seznamu
 * @param insert  datova structura member
 * @param after  datova structura member
 */
void insert_after(singleLinkedList *header, singleLinkedList insert, singleLinkedList after) {
	singleLinkedList head = *header;
	singleLinkedList next_after = NULL;

	if(!head) {
		*header = insert; 
		return;
	}

	if(after) {
		next_after = after->next;
	}
	
	while(head) {
		if(head->next == next_after) {
			singleLinkedList end = insert;
			if(!end) { break; }

			while(end->next) {
				end = end->next;
			}

			end->next = head->next;	
			head->next = insert;

			break;
		}

		head = head->next;
	}
}

/**
 * Vyjme ze seznamu z prvek clena seznamu r
 *
 * @param header adresa, z niz se bere anebo do niz se ma ulozit ukazatel na head - hlavu - prvniho clena seznamu
 * @param remove  datova structura member
 */
singleLinkedList pop(singleLinkedList *header, singleLinkedList remove) {
	singleLinkedList head = *header;
	singleLinkedList last = NULL;

	singleLinkedList next_remove = NULL;

	//if remove == NULL, it pops the last element of the singleLinkedList
	if(remove) {
		next_remove = remove->next;
	} 

	while(head) {
		if(head->next == next_remove) {
			if(last) {
				last->next = next_remove;
			} else {
				*header = head->next;
			}

			singleLinkedList return_value = head;
			head->next = NULL;

			return head;
		}

		last = head;
		head = head->next;
	}
}

/**
 * Vymaze cely seznamu - i. e. vsechny jeho cleny
 *
 * @param header adresa, z niz se bere anebo do niz se ma ulozit ukazatel na head - hlavu - prvniho clena seznamu
 */
void destroy(singleLinkedList *header) {
	singleLinkedList head = *header;
	singleLinkedList last = NULL;

	while(head) {
		if(last) {
			free(last);
		}

		last = head;
		head = head->next;
	}
	
	if(last) {
		free(last);
	}
}