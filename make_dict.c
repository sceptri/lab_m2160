#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include <database.h>

#define FILENAME_LENGTH 128

person load_person() {
	person input_person;
	printf("\tZadejte jmeno: ");
	scanf("%s", input_person.name);
	printf("\tZadejte prijmeni: ");
	scanf("%s", input_person.surname);
	printf("\tZadejte telefonni cislo: ");
	scanf("%u", &(input_person.phone_number));
	input_person.valid = true;
	printf("\n");

	return input_person;
}

database load() {
	database db;

	unsigned int input_count = 0;
	printf("Zadejte, prosim, pocet lidi: ");
	scanf("%u", &input_count);

	db.length = input_count;
	for(int i = 0; i < db.length; i++) {
		db.people[i] = load_person();
	}
	
	return db;
}

int main() {
	database db;
	db = load();

	FILE *file;

	printf("Zadejte jmeno souboru i s priponou: ");
	char filename[FILENAME_LENGTH];
	scanf("%s", filename);

	if((file = fopen(filename, "w")) == NULL) {
		printf("Nepodarilo se otevrit soubor \"%s\"\n", filename);
    	return EXIT_FAILURE;  
	}

	write_db_to_file(&db, file, false);
	fclose(file);

	return EXIT_SUCCESS;
}