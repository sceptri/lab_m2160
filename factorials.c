#include <stdio.h>

int factorial(int input) {
    if(input == 0) {
        return 1;
    } else {
        return input * factorial(input - 1);
    }
}

int main() {
    int number = 0;
    printf("Enter number: ");
    scanf("%d", &number);
    printf("Factorial is %d\n", factorial(number));

    return 0;
}