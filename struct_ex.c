#include <stdlib.h>
#include <stdio.h>

/**
 *  Priklad struktury a jejiho pouziti
 *
 * @file   structExamp.c
 * @author   Ales  Zlamal
 */

typedef struct tagData {
   unsigned char  den;
   unsigned char  mesic;
   unsigned int   rok;
} datum;

typedef struct tagAdresy {
   char          ulice[32];
   char          orientacniCislo[8];
   unsigned int  prvniCastPSC;
   unsigned int  druhaCastPSC;
   char          obec[32];
   char          zeme[32];
} adresa;

typedef struct tagOsoby {
   char   jmena[32];
   char   prijmeni[48];
   datum  datumNarozeni;
   char   mistoNarozeni[32];
   adresa bydliste;
} osoba;

/**
 * Nacte udaje o osobe
 * 
 * @return   vyplnenou strukturu osoba
 */
osoba nacti();

/**
 * Vypise udaje o osobe
 * 
 * @param   name  struktura osoba
 */
void vytiskni(osoba name);

int main()
{
    osoba zadatel;

    zadatel = nacti();
    printf("\n\nZadano bylo:\n");
    vytiskni(zadatel);
    return EXIT_SUCCESS;
}


osoba nacti()
{
    osoba z;    
    
    printf("Zadejte             (krestni jmen[o|a]: ");
    scanf(" %s", z.jmena);
    printf("                              prijmeni: ");
    scanf(" %s", z.prijmeni);
    printf("          cisly den mesic rok narozeni: ");
    scanf(" %hhu %hhu %u", &z.datumNarozeni.den, &z.datumNarozeni.mesic, &z.datumNarozeni.rok);
    printf("                        misto narozeni: ");
    scanf(" %s", z.mistoNarozeni);
    printf("         bydliste        -       ulice: ");
    scanf(" %s", z.bydliste.ulice);
    printf("         bydliste - (orientacni) cislo: ");
    scanf(" %s", z.bydliste.orientacniCislo);
    printf("         bydliste        -         PSC: ");
    scanf(" %u %u", &z.bydliste.prvniCastPSC, &z.bydliste.druhaCastPSC);
    printf("         bydliste        -        obec: ");
    scanf(" %s", z.bydliste.obec);
    printf("         bydliste        -        zeme: ");
    scanf(" %s", z.bydliste.zeme);
    return z;
}

void vytiskni(osoba name)
{
    printf("           (krestni jmen[o|a]: %s\n", name.jmena);
    printf("                     prijmeni: %s\n", name.prijmeni);
    printf(" cisly den mesic rok narozeni: %hhu %hhu %u\n", name.datumNarozeni.den, name.datumNarozeni.mesic, name.datumNarozeni.rok);
    printf("               misto narozeni: %s\n", name.mistoNarozeni);
    printf("bydliste        -       ulice: %s\n", name.bydliste.ulice);
    printf("bydliste - (orientacni) cislo: %s\n", name.bydliste.orientacniCislo);
    printf("bydliste        -         PSC: %u %u\n", name.bydliste.prvniCastPSC, name.bydliste.druhaCastPSC);
    printf("bydliste        -        obec: %s\n", name.bydliste.obec);
    printf("bydliste        -        zeme: %s\n", name.bydliste.zeme);
}

