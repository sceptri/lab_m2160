/**
 * Funkce parseujici retezec a program pro jeji demonstraci 
 *
 * @file   parsing.c
 * @author   Ales  Zlamal
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define LINE_SIZE 128
#define WORD_COUNT 4

/**
 * Rozdeli retezec na adrese source 
 * do pole retezcu, jehoz adresa je v promenne destination.
 * Ono pole retezcu je pole ukazatelu na useky pameti, v nich jsou ony retezce
 * Oddelovace jsou „mezerove” znaky - isspace - jeden i vice.
 * Velikost pole retezcu se podle potreby zvetsuje - dvoji.
 * Velikost pameti pro jednolive retezce se alokuji nejmensi potrebne.
 *
 * @param   destination  adresa pole retezcu, do nichz se rozdeli zadany znakovy retezec
 * @param   c  adresa promenne, v niz je ulozen pocet retezcu v poli
 * @param   source  znakovy retezec, ktery se ma rozdelit 
 * @return   aktualni pocet retezcu v poli
 */
int parse(char ***destination, int *c, char *source);

/**
 * Uvolni useky pameti pro retezce z pole retezcu, jehoz adresa je v destination.
 * Nasledne se uvolni i pamet pro ono pole ukazatelu na retezce.
 *
 * @param   destination  adresa pole retezcu
 * @param   c  promenna, v niz je ulozen pocet retezcu v poli
 */
void release (char **destination, int c);


int main()
{
    int i, word_count, default_word_count;
    char **words = NULL, line[LINE_SIZE];
    
    printf("Zadejte retezec: ");       // cte se dokud neni konec souboru (na unixovskych OS Ctrl-D na WinDOS Ctrl-Z) 
    while (fgets(line, LINE_SIZE, stdin) != NULL && *line != '\n') {                    // nebo prazdny radek - Enter
        *(line + strlen(line) - 1) = '\0';        // odstranime prechod na novy radek

        default_word_count = WORD_COUNT;
        word_count = parse(&words, &default_word_count, line);

        for (i = 0; i < word_count; ++i) {
            printf("|%s|\n", words[i]);
        }

        release(words, word_count);        
        printf("Zadejte retezec: ");
    } 
    if (*line != '\n') {
        printf("\n");
    }
    printf("Konec zadavani\n");
    return EXIT_SUCCESS;
}

int parse(char ***destination, int *c, char *source)
{
    char * current_char = source;
    int word_count = 0;
    int max_word_count = *c;

    char *** actual_destination = destination;

    *actual_destination = (char **) malloc(max_word_count * sizeof(char *));

    while(*current_char) { //When *current_char = '\0', it exits
        //Skip whitespaces
        while(isspace(*current_char)) {
            current_char++;
            //Continue se we check for terminator each iteration
            continue;
        }

        //mark word start 
        char * word_start = current_char;
        int word_length = 1;
        current_char++;
        
        //Skip space characters
        while(*current_char && !isspace(*current_char)) {
            current_char++;
            word_length++;
        }

        //If we ran out of word pointers, we reallocate words array
        if(word_count >= max_word_count) {
            max_word_count = word_count * 2;
            *actual_destination = (char **) realloc(*actual_destination, max_word_count * sizeof(char *));
        }

        //allocate enough chars to store our word (word length + terminating character)
        (*actual_destination)[word_count] = (char *) malloc((word_length + 1) * sizeof(char));
        current_char = word_start; //move pointer head to word start

        //Copy the word to the word_array
        for(int i = 0; i < word_length; i++) {
            (*actual_destination)[word_count][i] = *current_char;
            current_char++;
        }
        //add binary zero to the end of the word
        (*actual_destination)[word_count][word_length] = '\0';

        word_count++;
        current_char++;
    }


    return word_count;
}

void release (char **destination, int c)
{
    for(int i = 0; i < c; i++) {
        free(destination[i]);
    }

    free(destination);
}