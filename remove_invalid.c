#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include <database.h>

#define FILENAME_LENGTH 256

void clear_input_buffer() {
	int c;
	while ((c = getchar()) != '\n' && c != EOF) { }
}

bool no_one_invalid(database * db) {
	for(int i = 0; i < db->length; i++) {
		if(!db->people[i].valid) {
			return false;
		}
	}

	return true;
}

int main() {
	FILE *file;

	printf("Zadejte jmeno souboru i s priponou: ");
	char filename[FILENAME_LENGTH];
	scanf("%s", filename);

	if((file = fopen(filename, "r+")) == NULL) {
		printf("Nepodarilo se otevrit soubor \"%s\"\n", filename);
    	return EXIT_FAILURE;  
	}

	database db;
	db = load_from_file(file, false);

	if(no_one_invalid(&db)) {
		printf("Vsichni lide maji pridanou platnost!\n");
		return EXIT_SUCCESS;
	}

	printf("Tito lide budou odstraneni ze seznamu: \n");
	print_db(&db, false);

	clear_input_buffer();

	char answer;
	do {
		printf("Opravdu si prejete je odstranit?(a,n) ");
		scanf("%c", &answer);
	} while(answer != 'a' && answer != 'n');

	if(answer == 'a') {
		write_db_to_file(&db, file, true);
	}

	fclose(file);

	return EXIT_SUCCESS;

}