#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define NAME_LENGTH 256
#define PEOPLE_COUNT 32

typedef struct tagPerson {
	char name[NAME_LENGTH];
	char surname[NAME_LENGTH];
	unsigned int phone_number;
} person;

typedef struct tagDatabase {
	person people[PEOPLE_COUNT];
	int length;
} database;

/**
 * Loads given number of people into database and returns it
 * 
 * @return database struct with array of person given by user
*/
database load();

/**
 * Loads person as name, surname and phone number and returns it
 * 
 * @return person struct with given information
 */
person load_person();

/**
 * Searches given database for person which satisfies name
 * or if full name (if user entered it)
 * 
 * @param pointer to database with loaded information
 * @return returns index of person struct satisfying given info (-1 if not found)
*/
int find_person(database * db);

void print_person_number(database * db, int index);

int main() {
	database db;
	db = load();

	int found_at = find_person(&db);
	print_person_number(&db, found_at);

	return EXIT_SUCCESS;
}

person load_person() {
	person input_person;
	printf("\tZadejte jmeno: ");
	scanf("%s", input_person.name);
	printf("\tZadejte prijmeni: ");
	scanf("%s", input_person.surname);
	printf("\tZadejte telefonni cislo: ");
	scanf("%u", &(input_person.phone_number));
	printf("\n");

	return input_person;
}

database load() {
	database db;

	unsigned int input_count = 0;
	printf("Zadejte, prosim, pocet lidi: ");
	scanf("%u", &input_count);

	db.length = input_count;
	for(int i = 0; i < db.length; i++) {
		db.people[i] = load_person();
	}

	return db;
}

int find_person(database * db) {
	char input_name[2*NAME_LENGTH + 1];
	printf("Zadejte jmeno hledane osoby: ");
	scanf("%s", input_name);

	int found_index = -1;

	for(int i = 0; i < db->length; i++) {
		//We concat name and surname of person struct into: "name surname"
		char full_name[2*NAME_LENGTH + 1];
		strcpy(full_name, db->people[i].name);
		strcat(full_name, " ");
		strcat(full_name, db->people[i].surname);

		if(strcmp(input_name, db->people[i].name) == 0 || strcmp(input_name, full_name) == 0) {
			found_index = i;
		}
	}

	return found_index;
}

void print_person_number(database * db, int index) {
	if(index >= 0 && index < db->length) {
		printf("Telefonni cislo hledane osoby je: %u\n", db->people[index].phone_number);
	} else {
		printf("Osoba nebyla nalezena!\n");
	}
}