#pragma once

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define NAME_LENGTH 256
#define PEOPLE_COUNT 32

typedef struct tagPerson {
	char name[NAME_LENGTH];
	char surname[NAME_LENGTH];
	unsigned int phone_number;
	bool valid;
} person;

typedef struct tagDatabase {
	person people[PEOPLE_COUNT];
	int length;
} database;

// DATABASE LOADING AND WRITING

/**
 * Loads db from file
 * In the same sequence as it is written into the file
 * 
 * @param file file pointer to opened file to be read from
 * @param only_valid signals if only valid entries should be read from the file
 * @return database with loaded information
*/
database load_from_file(FILE * file, bool only_valid) {
	database db;
	int valid_index = 0;

	fread(&(db.length), sizeof(int), 1, file);
	for(int i = 0; i < db.length; i++) {
		person input_person;
		fread(input_person.name, NAME_LENGTH, 1, file);
		fread(input_person.surname, NAME_LENGTH, 1, file);
		fread(&(input_person.phone_number), sizeof(int), 1, file);
		fread(&(input_person.valid), sizeof(bool), 1, file);

		//Only valid people are properly loaded
		if(input_person.valid || !only_valid) {
			db.people[valid_index++] = input_person;
		}
	}

	//Indices start from 0
	db.length = valid_index;

	return db;
}

person load_and_search(FILE * file, bool only_valid) {
	
}


/**
 * Writes db to file
 * First, length of db is written, then each entry in the same sequence as they are stored
 * Same for each data entry of each person
 * 
 * @param db pointer to database with loaded information
 * @param file file pointer to opened file to be written into
 * @param only_valid signals if only valid entries should be written into the file
*/
void write_db_to_file(database * db, FILE * file, bool only_valid) {
	int count = only_valid ? 0 : db->length;

	//Determines the number of valid entries
	if(only_valid) {
		for(int i = 0; i < db->length; i++) {
			if(db->people[i].valid) {
				count++;
			}
		}
	}

	//move the file pointer to the beginning of the file
	rewind(file);

	fwrite(&(count), sizeof(int), 1 , file);
	for(int i = 0; i < db->length; i++) {
		if(db->people[i].valid || !only_valid) {
			fwrite(db->people[i].name, NAME_LENGTH, 1, file);
			fwrite(db->people[i].surname, NAME_LENGTH, 1, file);
			fwrite(&(db->people[i].phone_number), sizeof(int), 1, file);
			fwrite(&(db->people[i].valid), sizeof(bool), 1, file);
		}
	}
}

//SEARCHING AND PRINTING

/**
 * Searches given database for person which satisfies name
 * or if full name (if user entered it)
 * 
 * @param db pointer to database with loaded information
 * @param search_in bool to signal if it should find valid or invalid person
 * @return returns index of person struct satisfying given info (-1 if not found)
*/
int find_person(database * db, bool search_in) {
	char input_name[2*NAME_LENGTH + 1];
	printf("Zadejte jmeno hledane osoby: ");
	scanf("%s", input_name);

	int found_index = -1;

	for(int i = 0; i < db->length; i++) {
		if(db->people[i].valid == search_in) {
			//We concat name and surname of person struct into: "name surname"
			char full_name[2*NAME_LENGTH + 1];
			strcpy(full_name, db->people[i].name);
			strcat(full_name, " ");
			strcat(full_name, db->people[i].surname);

			if(strcmp(input_name, db->people[i].name) == 0 || strcmp(input_name, full_name) == 0) {
				found_index = i;
			}
		}
	}

	return found_index;
}

void print_db(database * db, bool search_in) {
	for(int i = 0; i < db->length; i++) {
		if(db->people[i].valid == search_in) {
			printf("%s %s %i \n", db->people[i].name, db->people[i].surname, db->people[i].phone_number);
		}
	}
}