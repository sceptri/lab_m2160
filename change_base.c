#include <stdio.h>

void change_base(int number, int basis) {
    if(number / basis == 0) {
        printf("%d", number % basis);
    } else {
        change_base(number / basis, basis);
        printf("%d", number % basis);
    }
}

int main() {
    int number, basis;
    printf("Enter number to change base: ");
    scanf("%d", &number);
    printf("Enter base to change to: ");
    scanf("%d", &basis);

    printf("Which is ");
    change_base(number, basis);
    printf("\n");
    
    return 0;
}