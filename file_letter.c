#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LINE_LENGTH 128
#define NAME_LENGTH 128

int main()
{
	FILE *file_old, *file_new;
	//Just in case, because we replace string 
	char line[2*LINE_LENGTH];

	printf("Zadejte jmeno souboru s priponou: ");
	char filename[NAME_LENGTH];
	scanf("%s",filename);
	
	if((file_old = fopen(filename, "r")) == NULL) {
		printf("Nepodarilo se otevrit soubor \"%s\"\n", filename);
    	return EXIT_FAILURE;  
	}

	printf("Jak se ma novy soubor jmenovat: ");
	scanf("%s",filename);

	if((file_new = fopen(filename, "w")) == NULL) {
		printf("Nepodarilo se vytvorit soubor \"%s\"\n", filename);
    	return EXIT_FAILURE;  
	}

	char reader[NAME_LENGTH], writer[NAME_LENGTH], * original_reader = "Julie", * original_writer = "Romeo";
	printf("Zadejte jmeno adresata: ");
	scanf("%s", reader);
	printf("Zadejte jmeno odesilajiciho: ");
	scanf("%s", writer);

	while(fgets(line, LINE_LENGTH, file_old) != NULL) {
		/**
		 * replacing name with a LONGER one causes OVERWRITE!!
		*/

		//first replace reader
		char * substring = strstr(line, original_reader);
		while(substring != NULL) {
			strncpy(substring, reader, strlen(reader));
			substring = strstr(line, original_reader);
		}
		//now for a writer
		substring = strstr(line, original_writer);
		while(substring != NULL) {
			strncpy(substring, writer, strlen(writer));
			substring = strstr(line, original_writer);
		}

		fputs(line, file_new);
	}

	fclose(file_new);
	fclose(file_old);

	return EXIT_SUCCESS;
}