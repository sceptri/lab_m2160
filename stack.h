#include "definedType.h"
#include "single_linked_list.h"

/**
 * Header file for stack operations
 * 
 * @file stack.h
 * @author   A.  Zlamal
 * @version 2020 04 15  
 * 
 */

typedef singleLinkedList stack;
/**
 *  Inicialize the given stack.
 *  
 *  Inicializuje zadany zasobnik.
 *
 *  @param[in, out]  s  the address of the stack to inicialize.
 *                      adresa zasobniku, jenz se ma inicialisovat.
 *                      
 *  @param[in]  sz  the size of the stack to inicialize. 
 *                  IN CASE OF IMPLEMENTATION BY LINKED LIST SIZE IS OMITTED.
 *                  velikost zasobniku, jenz se ma inicialisovat.
 *                  V PRIPADE IMPLEMENTACE SPOJOVYM SEZNAMEM SE VELIKOST OPOMIJI.
 */
void inicialisuj(stack *s, unsigned int sz){
	*s = NULL;
}

/**
 *  Push the value of the second parametr to the stack pointed by the first parametr.
 *  Vlozi hodnotu druheho parametru do zasobniku zadaneho prvním parametrem.   
 *
 *  @param[in, out]  s  the address of the stack, into which is to be pushed.
 *                      adresa zasobniku, do nejz se ma vlozit.
 *   
 *  @param[in]  pv  the address, from which is to load the pushed value.
 *                  adresa, z niz se ma do zasobniku vlozit hodnota na ni ulozena.
 *  IMPORTANT WARNING: REACTION ON THE PUSH TO FULL stack IS NOT DEFINED
 *                     AND WITH HIGH PROBABILITY CAUSES FATAL ERROR.
 *                     BEFORE PUSH TEST THE STACK BY FUNCTION jePlny.
 *   (Remark: there is the posibility to join jePlny and vloz
 *               int vlozJisteny(stack *s, valueType *pv)
 *               {
 *                   if (jePlny(s) {
 *                       return 1;
 *                   }
 *                   vloz(s, pv);
 *                   return 0;
 *               }, but I decided for the introduced variant.)
 *  DURAZNE UPOZORNENI: REAKCE NA VLOZENI DO PLNEHO ZASOBNIKU NENI DEFINOVANA
 *                      A S VYSOKOU PRAVDEPODOBNOSTI POVEDE K OSUDOVE CHYBE.
 *                      PRED VKLADANIM OTESTUJTE ZASOBNIK FUNKCI jePlny.
 *   (Poznamka: je mozne spojit jePlny a vloz
 *               int vlozJisteny(stack *s, valueType *pv)
 *               {
 *                   if (jePlny(s) {
 *                       return 1;
 *                   }
 *                   vloz(s, pv);
 *                   return 0;
 *               }, but I decided for the introduced variant.)
 */
void vloz(stack *s, valueType *pv) {
	stack insert = (stack) malloc(sizeof(member));
	memcpy(&(insert->value), pv, sizeof(valueType));
	insert->next = NULL;

	insert_after(s, insert, NULL);
}

/**
 *  Pop the value from the stack pointed by the first parametr. 
 *  and store it into address given by the second parametr.
 *  
 *  Vyjme hodnotu ze zasobniku zadaneho prvním parametrem 
 *  a ulozi ji na adresu danou druhym parametrem.   
 *   
 *  @param[in, out]  s  the address of the stack, from which is to be poped.
 *                      adresa zasobniku, z nejz se ma vybrat.
 *  
 *  @param[out]  pv  the address, into which is to store the poped value.
 *                   adresa, na niz se ma vybrana hodnota ulozit.
 *
 *  IMPORTANT WARNING: REACTION ON THE POP FROM EMPTY STACK IS NOT DEFINED
 *                        AND WITH HIGH PROBABILITY CAUSES FATAL ERROR.
 *                        BEFORE POP TEST THE stack BY FUNCTION jePrazdny.
 *   (Remark: there is the posibility to join stackIsEmpty and pop
 *               int vyjmiJisteny(stack *s, valueType *pv)
 *               {
 *                   if (jePrazdny(q) {
 *                       return 1;
 *                   }
 *                   vyjmi(s, pv);
 *                   return 0;
 *               }, but I decided for the introduced variant.)
 *  DURAZNE UPOZORNENI: REAKCE NA VYBER Z PRAZDNEHO ZASOBNIKU NENI DEFINOVANA
 *                      A S VYSOKOU PRAVDEPODOBNOSTI POVEDE K OSUDOVE CHYBE.
 *                      PRED VYBEREM OTESTUJTE ZASOBNIK FUNKCI jePrazdny.
 *   (Poznamka: je mozne spojit stackIsFull a pop
 *               int vyjmiJisteny(stack *s, demanded_t *pv)
 *               {
 *                   if (jePrazdny(q) {
 *                       return 1;
 *                   }
 *                   vyjmi(s, pv);
 *                   return 0;
 *               }, but I decided for the introduced variant.)
 */
void vyjmi(stack *s, valueType *pv) {
	stack popped = (stack) pop(s, NULL);
	memcpy(pv, &(popped->value), sizeof(valueType));
	destroy(&popped);
}

/**
 *  Returns the value, which is on the top of the stack. 
 *  Vrati hodnotu, jez je na vrcholu zasobniku. 
 *
 *  @param[in, out]  s  the address of the stack, from which is to be copied the value on it's top.
 *                      adresa zasobniku, z jehoz vrcholu se ma zkopirovat hodnota.
 *  
 *  @param[out]  pv  the address, into which is to store the value, which is on it's top.
 *                   adresa, na niz se ma hodnota z jeho vrcholu ulozit.
 *
 *  IMPORTANT WARNING: REACTION ON THE REFERENCING THE TOP OF THE EMPTY stack IS NOT DEFINED
 *                     AND WITH HIGH PROBABILITY CAUSES FATAL ERROR.
 *                     BEFORE USING TOP TEST THE STACK BY FUNCTION jePrazdny.
 *   (Remark: there is the posibility to join jePrazdny and vrchol
 *               int vrcholJisteny(stack *s, valueType *pv)
 *               {
 *                   if (jePrazdny(q) {
 *                       return 1;
 *                   }
 *                   vrchol(s, pv);
 *                   return 0;
 *               }, but I decided for the introduced variant.)
 *  DURAZNE UPOZORNENI: REAKCE NA ODKAZ NA VRCHOL PRAZDNEHO ZASOBNIKU NENI DEFINOVANA
 *                      A S VYSOKOU PRAVDEPODOBNOSTI POVEDE K OSUDOVE CHYBE.
 *                      PRED REFERENCOVANIM VRCHOLU OTESTUJTE ZASOBNIK FUNKCI jePrazdny.
 *   (Poznamka: je mozne spojit jePrazdny a vrchol
 *               int vrcholJisteny(stack *s, valueType *pv)
 *               {
 *                   if (jePrazdny(q) {
 *                       return 1;
 *                   }
 *                   vrchol(s, pv);
 *                   return 0;
 *               }, but I decided for the introduced variant.)
 */
void vrchol(stack *s, valueType *v) {
	stack head = *s;
	stack last = NULL;
	while(head) {
		last = head;
		head = head->next;
	}

	memcpy(v, &(last->value), sizeof(valueType));
}

/**
 *  Tests if the stack is empty. 
 *  Testuje zda je zasobnik prazdny. 
 *   
 *  @param[in, out]  s  the address of the stack.
 *                      adresa zasobniku.
 *   
 *  @return  non zero iff the stack is empty; otherwise zero.
 *           nenulovou hodnotu, jen je-li zasobnik prazdny; v opacnem pripade nulu. 
 */
int jePrazdny(stack *s) {
	return (*s == NULL);
}

/**
 *  Tests if the stack is full. 
 *  Testuje zda je zasobnik plny.  
 *
 *  @param[in, out]  s  the address of the stack.
 *                      adresa zasobniku.
 *   
 *  @return  zero iff the stack isn't full; otherwise non zero.
 *           nulu jen neni-li zasobnik plny; v opacnem pripade nenulovou hodnotu.
 */
int jePlny(stack *s) {
	return 0;
}

/**
 *  Free the memory allocated for the stack in case of implentation by linked list.
 *                  (The same effect as initialize, but without memory leaks tested by valgrind.)
 *  Uvolni pamet alokovanou pro zasobnik v pripade implementace spojovym seznamem.
 *                  (Stejny efekt jako inicialisuj, ale bez memory leaku zjistovanych valgrindem.)
 *   
 *  @param[in, out]  s  the address of the stack.
 *                      adresa zasobniku.
 */
void uvolniPamet(stack *s) {
	destroy(s);
}