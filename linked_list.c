#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "linked_list.h"

int main() {

	linkedList header;

	load(&header, stdin);

	pop(&header, header);

	print(&header);

	return EXIT_SUCCESS;
}