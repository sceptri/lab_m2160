#include <stdio.h>
#include <string.h>

// Min and max values are inclusive
int int_get_input(char * message, int min_value, int max_value) {
	int input = 0;
	do{
		printf("%s", message);
		scanf("%d", &input);
	} while(input < min_value || input > max_value);

	return input;
}

void month_to_genitive(char * month, char * month_genitive) {
	strcpy(month_genitive, month);
	int len = strlen(month);

	char * last_char = month_genitive + (len - 1);
	char * penultimate_char = month_genitive + (len - 2);
	char * next_char = month_genitive + len;
	char * append_break = month_genitive + (len + 1);

	if(*last_char == 'n' && *penultimate_char == 'e') {
		*penultimate_char = 'n';
		*last_char = 'a';
	} else if (*last_char == 'c' && *penultimate_char == 'e') { //prosinec, cervenec
		*penultimate_char = 'c';
		*last_char = 'e';
	} else if (*last_char == 'r') { //unor
		*next_char = 'a';
		*append_break = '\0';
	} else if (*last_char == 'i') { //zari
		//nothing
	} else if (*last_char == 'd') { //listopad
		*next_char = 'u';
		*append_break = '\0';
	}
}

int main() {
	char * days_in_week[] = {"pondeli", "utery", "streda", "ctvrtek", "patek", "sobota", "nedele"},
		 * months_in_year[] = {"leden", "unor", "brezen", "duben", "kveten", "cerven", "cervenec", "srpen", "zari", "rijen", "listopad", "prosinec"};

	int input_day_in_week = int_get_input("Zadejte, prosim, den v tydnu cislici: ", 1, 7);
	int input_day_in_month = int_get_input("Zadejte, prosim, den v mesici cislici: ", 1, 31);
	int input_month = int_get_input("Zadejte, prosim, mesic v roce cislici: ", 1, 12);
	int input_year = int_get_input("Zadejte, prosim, rok cislici: ", 0, 9999);

	//Longest genitive is "listopadu"... for which we need array of 10 elements
	char month_genitive[10];
	month_to_genitive(months_in_year[input_month - 1], month_genitive);

	//Output format: den +, + _  + cislo + . + mesic + _ + rok + \n + \0
	//7 + 1 + 1 + 2 + 1 + 10 + 1 + 4 + 1 + 1 = 29
	char output[29];
	sprintf(output, "%s, %d.%s %d\n", days_in_week[input_day_in_week - 1], input_day_in_month, month_genitive, input_year);

	printf("%s", output);

	return 0;
}