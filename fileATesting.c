/**
 * Funkce pro ukazku cteni ze souboru 
 *
 * @file   fileATesting.c
 * @author   Ales  Zlamal
 */
#include <stdlib.h>
#include <stdio.h>

#define LINE_LENGTH 128

int main()
{
    FILE *f;
    char line[LINE_LENGTH];
    
    if ((f = fopen("testingFile.txt", "r")) == NULL) {
        printf("Nepodarilo se otevrit soubor \"testingFile.txt\"\n");
        return EXIT_FAILURE;   
    }    
    while (fgets(line, LINE_LENGTH, f) != NULL) {
        printf(">>> %s", line);
        *(line + 4) = '/';
        printf(">>> %s\n", line);
    }
    printf("konec\n");
    fclose(f);
    return EXIT_SUCCESS;   
}