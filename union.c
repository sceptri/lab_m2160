#include <stdlib.h>
#include <stdio.h>

/**
 *  Priklad unionu a jeho pouziti
 *
 * @file   unionExamp.c
 * @author   Ales  Zlamal
 */

typedef union tagNumber {
   unsigned long  longNumber;
   unsigned char  bytes[8];
} number;


int main()
{
    number n;

    printf("%zu\n", sizeof(long int));    
    n.longNumber = 0x0123456789ABCDEFL;
    printf("|                |\n|");    
    for (int i = 0; i < 8; ++i) {
        printf("%02hhX", n.bytes[i]);    
    }
    printf("|\n");    
    return EXIT_SUCCESS;
}