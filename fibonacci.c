#include <stdio.h>

int fibonacci(unsigned int level) {
    if (level < 2) {
        return level;
    } else {
        return fibonacci(level - 1) + fibonacci(level - 2);
    }
}

int main() {
    int level = 0;
    printf("Enter level: ");
    scanf("%d", &level);
    printf("%d-th element of fibonacci sequence is %d\n", level, fibonacci(level));

    return 0;
}