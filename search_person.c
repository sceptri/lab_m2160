#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include <database.h>

#define FILENAME_LENGTH 128

void print_person_number(database * db, int index);

int main() {
	FILE *file;

	printf("Zadejte jmeno souboru i s priponou: ");
	char filename[FILENAME_LENGTH];
	scanf("%s", filename);

	if((file = fopen(filename, "r")) == NULL) {
		printf("Nepodarilo se otevrit soubor \"%s\"\n", filename);
    	return EXIT_FAILURE;  
	}

	database db;
	db = load_from_file(file, true);

	int found_at = find_person(&db, true);
	print_person_number(&db, found_at);

	fclose(file);

	return EXIT_SUCCESS;
}

void print_person_number(database * db, int index) {
	if(index >= 0 && index < db->length) {
		printf("Telefonni cislo hledane osoby je: %u\n", db->people[index].phone_number);
	} else {
		printf("Osoba nebyla nalezena!\n");
	}
}