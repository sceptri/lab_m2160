#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "definedType.h"
#include "stack.h"

/**
 * File with function main for demonstration stack operations
 *
 * @file main.c 
 * @author   A.  Zlamal
 * @version 2020 04 15  
 * 
 */

int main()
{
    stack z;
    valueType y;
    int isf;

    inicialisuj(&z, 8); 
    printf("Zadavejte na radky znakove retezce. Ta se budou vkladat do zasobniku.\n"
           "Zadavani se ukoncuje prazdnym radkem.\n");
    while ((fgets(y, TEXT_LENGTH, stdin) != NULL) && (*y != '\n') && !(isf = jePlny(&z))) {
        y[strlen(y) - 1] = '\0';
        vloz(&z, &y);
    }
    if (isf) {
        printf("Zasobnik je plny: hodnota %s byla posledni, ktera se jeste vesla.\n", y);
    }
    printf("Konec vkladani do zasobniku.\n");
    printf("Nejvyse dvakrat vyber ze zasobniku:\n");
    if (!jePrazdny(&z)) {
        vrchol(&z, &y);
        printf("    %s :: ", y);
        vyjmi(&z, &y);
        printf("%s\n", y);
    }
    if (!jePrazdny(&z)) {
        vrchol(&z, &y);
        printf("    %s :: ", y);
        vyjmi(&z, &y);
        printf("%s\n", y);
    }
    printf("Nasleduje dalsi vkladani do zasobniku:\n");
    while( (fgets(y, TEXT_LENGTH, stdin) != NULL) && (*y != '\n') && !(isf = jePlny(&z))) {
        y[strlen(y) - 1] = '\0';
        vloz(&z, &y);
    }
    if (isf) {
        printf("Zasobnik je plny: hodnota %s byla posledni, ktera se jeste vesla.\n", y);
    }
    printf("Vyber vsech hodnot ze zasobniku:\n");
    while (!jePrazdny(&z)) {
        vrchol(&z, &y);
        printf("    %s :: ", y);
        vyjmi(&z, &y);
        printf("%s\n", y);
    }
    printf("\nNasleduje dalsi vkladani do zasobniku a opet vyber vseho z nej:\n");
    while( (fgets(y, TEXT_LENGTH, stdin) != NULL) && (*y != '\n') && !(isf = jePlny(&z))) {
        y[strlen(y) - 1] = '\0';
        vloz(&z, &y);
    }
    if (isf) {
        printf("Zasobnik je plny: hodnota %s byla posledni, ktera se jeste vesla.\n", y);
    }
    printf("Vyber vsech hodnot ze zasobniku:\n");
    while (!jePrazdny(&z)) {
        vrchol(&z, &y);
        printf("    %s :: ", y);
        vyjmi(&z, &y);
        printf("%s\n", y);
    }
    uvolniPamet(&z);
    printf("Byla uvolnena pamet zasobniku: %s.\n", z == NULL ? "NULL" : z->value);
    return EXIT_SUCCESS;
}