#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <database.h>

#define FILENAME_LENGTH 128

int main() {
	FILE *file;

	printf("Zadejte jmeno souboru i s priponou: ");
	char filename[FILENAME_LENGTH];
	scanf("%s", filename);

	if((file = fopen(filename, "r")) == NULL) {
		printf("Nepodarilo se otevrit soubor \"%s\"\n", filename);
    	return EXIT_FAILURE;  
	}

	database db = load_from_file(file, true);
	print_db(&db, true);

	fclose(file);

	return EXIT_SUCCESS;
}