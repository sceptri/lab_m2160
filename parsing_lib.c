/**
 * Funkce parseujici retezec a program pro jeji demonstraci 
 *
 * @file   parsing.c
 * @author   Ales  Zlamal
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define LINE_SIZE 512
#define WORD_COUNT 4

/**
 * Rozdeli retezec na adrese source 
 * do pole retezcu, jehoz adresa je v promenne destination.
 * Ono pole retezcu je pole ukazatelu na useky pameti, v nich jsou ony retezce
 * Oddelovace jsou „mezerove” znaky - isspace - jeden i vice.
 * Velikost pole retezcu se podle potreby zvetsuje - dvoji.
 * Velikost pameti pro jednolive retezce se alokuji nejmensi potrebne.
 *
 * @param   destination  adresa pole retezcu, do nichz se rozdeli zadany znakovy retezec
 * @param   c  adresa promenne, v niz je ulozen pocet retezcu v poli
 * @param   source  znakovy retezec, ktery se ma rozdelit 
 * @return   aktualni pocet retezcu v poli
 */
int parse(char ***destination, int *c, char *source);

int specific_parse(char ***destination, int *c, char *source, char delimiter);

void copy_word();

/**
 * Uvolni useky pameti pro retezce z pole retezcu, jehoz adresa je v destination.
 * Nasledne se uvolni i pamet pro ono pole ukazatelu na retezce.
 *
 * @param   destination  adresa pole retezcu
 * @param   c  promenna, v niz je ulozen pocet retezcu v poli
 */
void release (char **destination, int c);


int main()
{
    int i, word_count, default_word_count;
    char **words = NULL, line[LINE_SIZE];
    
    printf("Zadejte retezec: ");       // cte se dokud neni konec souboru (na unixovskych OS Ctrl-D na WinDOS Ctrl-Z) 
    while (fgets(line, LINE_SIZE, stdin) != NULL && *line != '\n') {                    // nebo prazdny radek - Enter
        *(line + strlen(line) - 1) = '\0';        // odstranime prechod na novy radek

        default_word_count = WORD_COUNT;
        word_count = parse(&words, &default_word_count, line);

        for (i = 0; i < word_count; ++i) {
            printf("|%s|\n", words[i]);
        }

        release(words, word_count);        
        printf("Zadejte retezec: ");
    } 
    if (*line != '\n') {
        printf("\n");
    }
    printf("Konec zadavani\n");
    return EXIT_SUCCESS;
}

int specific_parse(char *** destination, int * c, char * source, char delimiter) {

    char * next_delimiter = strchr(source, delimiter);
    char * word_start = source;
    int word_count = 0;
    int max_word_count = * c;

    *destination = (char **) malloc(max_word_count * sizeof(char *));

    while(next_delimiter != NULL) {
        int interval_length = next_delimiter - word_start; 
        if(interval_length <= 1) {
            word_start = next_delimiter + 1;
            next_delimiter = strchr(word_start, delimiter);
            continue;
        }

       copy_word(destination, word_start, interval_length, &max_word_count, word_count);

        word_count++;
        word_start = next_delimiter + 1;
        next_delimiter = strchr(word_start, delimiter);
    }

    //If there are remaining chars to be parsed
    int remaining_chars_count = strlen(word_start);
    if(remaining_chars_count != 0) {
        copy_word(destination, word_start, remaining_chars_count, &max_word_count, word_count);
        word_count++;
    }

    return word_count;
}

void copy_word(char *** destination, char * source, int word_length, int * allocated_word_space, int used_word_space)
{
    //If we ran out of word pointers, we reallocate words array
    if(used_word_space >= *allocated_word_space) {
        *allocated_word_space = (used_word_space) * 2;
        *destination = (char **) realloc(*destination, (*allocated_word_space) * sizeof(char *));
    }

    //allocate enough chars to store our word (interval length + terminating character)
    (*destination)[used_word_space] = (char *) malloc((word_length + 1) * sizeof(char));

    strncpy((*destination)[used_word_space], source, word_length);
    //add binary zero to the end of the word
    (*destination)[used_word_space][word_length] = '\0';
}


int parse(char ***destination, int *c, char *source)
{
    //sequence of deilimiters we use to parse our text against (in this order)
    char * delimiters_sequence = {':',';', ',', ' '};
    int word_count = *c;
    char * actual_source = (char *) malloc((strlen(source) + 1) * sizeof(char));
    strcpy(actual_source, source);

    for(int i = 0; i < 4; i++) {
        //We release words from previous iteration
        if(i != 0) {
            release(*destination, word_count);
        }
        word_count = specific_parse(destination, &word_count, actual_source, delimiters_sequence[i]);

        //we put words we parsed in the last parse into new line that we will parse in the next iteration
        for(int j = 0; j < word_count; j++) {
            if(j == 0) {
                strcpy(actual_source, (*destination)[j]);
            } else {
                strcat(actual_source, (*destination)[j]);
            }
        }
    }

    free(actual_source);

    return word_count;
}

void release (char **destination, int c)
{
    for(int i = 0; i < c; i++) {
        free(destination[i]);
    }

    free(destination);
}