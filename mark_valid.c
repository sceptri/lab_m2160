#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include <database.h>

#define FILENAME_LENGTH 128

int main() {
	FILE *file;

	printf("Zadejte jmeno souboru i s priponou: ");
	char filename[FILENAME_LENGTH];
	scanf("%s", filename);

	if((file = fopen(filename, "r+")) == NULL) {
		printf("Nepodarilo se otevrit soubor \"%s\"\n", filename);
    	return EXIT_FAILURE;  
	}

	database db;
	db = load_from_file(file, false);

	int found_at = find_person(&db, false);
	if(found_at >= 0 && found_at < db.length) {
		db.people[found_at].valid = true;
		printf("Osobe %s %s byla pridana platnost!\n", db.people[found_at].name, db.people[found_at].surname);
		
		write_db_to_file(&db, file, false);
	}else{
		printf("Osoba nebyla nalezena!\n");
	}

	fclose(file);

	return EXIT_SUCCESS;
}