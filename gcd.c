#include <stdio.h>

int greatest_common_divisor(int u, int w) {
    if(w != 0) {
        return greatest_common_divisor(w, u % w);
    } else {
        return u;
    }
}

int main() {
    int u,w;
    printf("Enter first number: ");
    scanf("%d", &u);
    printf("Enter second number: ");
    scanf("%d", &w);

    printf("Greatest common divisor of %d and %d is %d\n", u, w, greatest_common_divisor(u, w));

    return 0;
}